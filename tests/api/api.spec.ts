import { expect, test } from 'vitest'
import api from '../../framework/api'
import jsonSchemas from '../../framework/json-schemas'

test('Gets list of valid vacancies', async () => {
  const response = await api.getVacancies(1)
  let vacanciesList: object[] = []
  if (response.status === 200) {
    vacanciesList = (await response.json()).list
  }
  vacanciesList.forEach((vacancy) => {
    expect(jsonSchemas.vacancies.vacancy(vacancy)).toBeTruthy()
  })
})

test('Gets list of valid resumes', async () => {
  const response = await api.getResumes(1)
  let resumesList: object[] = []
  if (response.status === 200) {
    resumesList = (await response.json()).list
  }
  resumesList.forEach((resume) => {
    expect(jsonSchemas.resumes.resume(resume)).toBeTruthy()
  })
})

test('Gets list of valid Habr resumes', async () => {
  const response = await api.getHabrResumes(1)
  let habrResumesList: object[] = []
  if (response.status === 200) {
    habrResumesList = (await response.json()).list
  }
  habrResumesList.forEach((habrResume) => {
    expect(jsonSchemas.habrResumes.resume(habrResume)).toBeTruthy()
  })
})

test('Gets list of valid Q&A resumes', async () => {
  const response = await api.getQAResumes(1)
  let qaResumesList: object[] = []
  if (response.status === 200) {
    qaResumesList = (await response.json()).list
  }
  qaResumesList.forEach((qaResume) => {
    expect(jsonSchemas.qaResumes.resume(qaResume)).toBeTruthy()
  })
})

test('Gets list of valid experts', async () => {
  const response = await api.getExperts(1)
  let expertsList: object[] = []
  if (response.status === 200) {
    expertsList = (await response.json()).list
  }
  expertsList.forEach((expert) => {
    expect(jsonSchemas.experts.expert(expert)).toBeTruthy()
  })
})
