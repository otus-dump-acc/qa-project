import { test, expect } from '@playwright/test'
import VacanciesPage from '../../framework/pages/Vacancies'
import VacancyCard from '../../framework/components/VacancyCard'

test('The homes page opens and has visible content.', async ({ page }) => {
  const vacanciesPage = new VacanciesPage(page)
  await vacanciesPage.visit()
  expect(vacanciesPage.page.url()).toContain(vacanciesPage.PAGE_URL)
  await expect(vacanciesPage.pageTitle).toBeVisible()
})

test('Link of vacancy card redirects to the vacancy page.', async ({ page }) => {
  const vacanciesPage = new VacanciesPage(page)
  await vacanciesPage.visit()
  const vacancyLocator = await vacanciesPage.getVacancyCardByOrder(0)
  const firstVacancyCard = new VacancyCard(vacancyLocator)
  const vacancyPage = await firstVacancyCard.openVacancyPage()
  await expect(vacancyPage.pageTitle).toBeVisible()
})

test('Specs filter modal window opens on field click.', async ({ page }) => {
  const vacanciesPage = new VacanciesPage(page)
  await vacanciesPage.visit()
  const filtersList = vacanciesPage.filtersList
  await expect(filtersList.specsModal).toBeHidden()
  await filtersList.openSpecsModal()
  await expect(filtersList.specsModal).toBeVisible()
})

test('Select specs filter then clear it.', async ({ page }) => {
  const vacanciesPage = new VacanciesPage(page)
  await vacanciesPage.visit()
  await expect(vacanciesPage.selectedFilters).toBeHidden()
  await vacanciesPage.selectFirstSpecFilter()
  await expect(vacanciesPage.selectedFilters).toBeVisible()
  await vacanciesPage.clearFilters()
  await expect(vacanciesPage.selectedFilters).toBeHidden()
})

test('Show sallary checkbox activates vacancy salllary.', async ({ page }) => {
  const vacanciesPage = new VacanciesPage(page)
  await vacanciesPage.visit()
  await vacanciesPage.toggleVacancyBasicSalary()
  const vacanciesCardList = await vacanciesPage.getVacanciesCardList()
  vacanciesCardList.forEach(async (locator) => {
    const vacancyCard = new VacancyCard(locator)
    await expect(vacancyCard.basicSalary).toBeVisible()
  })
})
