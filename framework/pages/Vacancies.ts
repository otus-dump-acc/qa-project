import { Locator, Page } from '@playwright/test'
import BasePage from './Base'
import FiltersList from '../components/FiltersList'
import { vacancyCardLocators } from '../components/VacancyCard'

export const vacanciesPageLocators = {
  pageTitle: '.page-title__title',
  filtersList: '.list-page-sidebar__filters > .section-group',
  selectedFilters: '.selected-filters__items',
  clearFiltersButton: '.filters-controls > button:nth-child(1)',
}

export default class VacanciesPage extends BasePage {
  #PAGE_URL = '/vacancies'

  constructor(page: Page) {
    super(page)
  }

  get pageTitle(): Locator {
    return this.page.locator(vacanciesPageLocators.pageTitle)
  }

  get PAGE_URL(): string {
    return this.#PAGE_URL
  }

  get vacanciesList() {
    return this.page
  }

  get filtersList() {
    return new FiltersList(this.page.locator(vacanciesPageLocators.filtersList))
  }

  get selectedFilters() {
    return this.page.locator(vacanciesPageLocators.selectedFilters)
  }

  get clearFiltersButton() {
    return this.page.locator(vacanciesPageLocators.clearFiltersButton).first()
  }

  async visit(): Promise<VacanciesPage> {
    await this.page.goto(this.#PAGE_URL)
    await this.page.waitForLoadState('domcontentloaded')
    return this
  }

  async getVacancyCardByOrder(orderNumber: number): Promise<Locator> {
    return (await this.page.locator(vacancyCardLocators.card).all())[orderNumber]
  }

  async getVacanciesCardList(): Promise<Locator[]> {
    return await this.page.locator(vacancyCardLocators.card).all()
  }

  async selectFirstSpecFilter() {
    await this.filtersList.selectFirstSpecsFilter()
  }

  async toggleVacancyBasicSalary() {
    await this.filtersList.toggleShowBasicSalary()
  }

  async clearFilters() {
    await this.clearFiltersButton.click()
  }
}
