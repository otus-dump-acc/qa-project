import { Page } from '@playwright/test'
import BasePage from './Base'

export const vacancyPageLocators = {
  pageTitle: '//h1[contains(@class, "page-title__title")]',
}

export default class VacancyPage extends BasePage {
  #PAGE_URL = '/vacancies'

  constructor(page: Page) {
    super(page)
  }

  get pageTitle() {
    return this.page.locator(vacancyPageLocators.pageTitle)
  }

  pageUrl(id: number | string) {
    return this.#PAGE_URL + '/' + id
  }

  async visit(id: number | string): Promise<VacancyPage> {
    await this.page.goto(this.pageUrl(id))
    await this.page.waitForLoadState('domcontentloaded')
    return this
  }
}
