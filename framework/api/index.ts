interface BuildEndpointParams {
  url: string
  headers?: object
  body?: object | any[] | string
  method?: 'GET' | 'POST' | 'PUT' | 'DELETE'
}

const BASE_URL = process.env.BASE_URL
const API_VACANCIES_PATH = process.env.API_VACANCIES_PATH
const API_RESUMES_PATH = process.env.API_RESUMES_PATH
const API_HABR_RESUMES_PATH = process.env.API_HABR_RESUMES_PATH
const API_QA_RESUMES_PATH = process.env.API_QA_RESUMES_PATH
const API_EXPERTS_PATH = process.env.API_EXPERTS_PATH

function buildEndpoint({ url, headers, body, method = 'GET' }: BuildEndpointParams) {
  return fetch(url, {
    headers: new Headers(Object.assign({ Accept: '*/*' }, headers)),
    body: body !== undefined ? JSON.stringify(body) : undefined,
    method,
  })
}

export default {
  getVacancies: (pageNumber: number) =>
    buildEndpoint({
      url:
        (BASE_URL || '') +
        (API_VACANCIES_PATH || '') +
        `?sort=relevance&type=all&with_salary=1&currency=RUR&page=${pageNumber}`,
    }),
  getResumes: (pageNumber: number) =>
    buildEndpoint({
      url:
        (BASE_URL || '') +
        (API_RESUMES_PATH || '') +
        `?order=last_visited&currency=RUR&page=${pageNumber}`,
    }),
  getHabrResumes: (pageNumber: number) =>
    buildEndpoint({
      url: (BASE_URL || '') + (API_HABR_RESUMES_PATH || '') + `?page=${pageNumber}`,
    }),
  getQAResumes: (pageNumber: number) =>
    buildEndpoint({
      url: (BASE_URL || '') + (API_QA_RESUMES_PATH || '') + `?page=${pageNumber}`,
    }),
  getExperts: (pageNumber: number) =>
    buildEndpoint({
      url: (BASE_URL || '') + (API_EXPERTS_PATH || '') + `order=lastActive&page=${pageNumber}`,
    }),
}
