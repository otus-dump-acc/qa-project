import Ajv from 'ajv'
import { JTDSchemaType } from 'ajv/dist/core'

interface IExpertJSONScheme {
  title: string
  login: string
  alias: string
  avatar: {
    src: string
    src2x: string
  }
  age: {
    title: string
    value: number
  } | null
  experience: {
    title: string
    value: number
  }
  lastJob: {
    position: string
    company: {
      title: string
      accredited?: boolean
      href: string | null
    }
    duration: {
      title: string
      value: number
    }
  } | null
  lastVisited: {
    title: string
    date: string
  }
  score: {
    connections: number
    averageScore: number
    scoresCount: number
  }
  qualifications: {
    title: string
    position: number
    href: string
  }[]
  specializations: {
    title: string
    href: string
  }[]
  skills: {
    title: string
    href: string
  }[]
  requests: {
    title: string
    items: {
      title: string
      href: string
    }[]
  }[]
  rate: {
    amount: number
    currency: string
    freeIntro: boolean
  }
  connectHref: string
  hasDialog: boolean
}

const ExpertJSONScheme: JTDSchemaType<IExpertJSONScheme> = {
  properties: {
    title: {
      type: 'string',
    },
    login: {
      type: 'string',
    },
    alias: {
      type: 'string',
    },
    avatar: {
      properties: {
        src: {
          type: 'string',
        },
        src2x: {
          type: 'string',
        },
      },
    },
    age: {
      properties: {
        title: {
          type: 'string',
        },
        value: {
          type: 'uint16',
        },
      },
      nullable: true,
    },
    experience: {
      properties: {
        title: {
          type: 'string',
        },
        value: {
          type: 'uint16',
        },
      },
    },
    lastJob: {
      properties: {
        position: {
          type: 'string',
        },
        company: {
          properties: {
            title: {
              type: 'string',
            },
            href: {
              type: 'string',
              nullable: true,
            },
          },
          optionalProperties: {
            accredited: {
              type: 'boolean',
            },
          },
        },
        duration: {
          properties: {
            title: {
              type: 'string',
            },
            value: {
              type: 'uint16',
            },
          },
        },
      },
      nullable: true,
    },
    lastVisited: {
      properties: {
        title: {
          type: 'string',
        },
        date: {
          type: 'string',
        },
      },
    },
    score: {
      properties: {
        connections: {
          type: 'uint32',
        },
        averageScore: {
          type: 'float32',
        },
        scoresCount: {
          type: 'uint32',
        },
      },
    },
    qualifications: {
      elements: {
        properties: {
          title: {
            type: 'string',
          },
          position: {
            type: 'uint8',
          },
          href: {
            type: 'string',
          },
        },
      },
    },
    specializations: {
      elements: {
        properties: {
          title: {
            type: 'string',
          },
          href: {
            type: 'string',
          },
        },
      },
    },
    skills: {
      elements: {
        properties: {
          title: {
            type: 'string',
          },
          href: {
            type: 'string',
          },
        },
      },
    },
    requests: {
      elements: {
        properties: {
          title: {
            type: 'string',
          },
          items: {
            elements: {
              properties: {
                title: {
                  type: 'string',
                },
                href: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
    rate: {
      properties: {
        amount: {
          type: 'uint32',
        },
        currency: {
          type: 'string',
        },
        freeIntro: {
          type: 'boolean',
        },
      },
    },
    connectHref: {
      type: 'string',
    },
    hasDialog: {
      type: 'boolean',
    },
  },
}

export default function experts(ajv: Ajv) {
  return {
    expert: ajv.compile(ExpertJSONScheme),
  }
}
