import Ajv from 'ajv'
import { JTDSchemaType } from 'ajv/dist/core'

interface IVacancyJSONScheme {
  id: number
  href: string
  title: string
  isMarked: boolean
  remoteWork: boolean
  salaryQualification: {
    title: string
    href: string
  } | null
  publishedDate: {
    date: string
    title: string
  }
  location: any | null
  company: {
    alias_name: string
    href: string
    title: string
    accredited: boolean
    logo: {
      src: string
    }
    rating: {
      title: string
      value: string
      href: string
    } | null
  }
  employment: 'full_time' | 'part_time' | null
  salary: {
    from: number | null
    to: number | null
    currency: string
    formatted: string
  }
  divisions: {
    title: string
    href: string
  }[]
  skills: {
    title: string
    href: string
  }[]
  media: any | null
  locations:
    | {
        title: string
        href: string
      }[]
    | null
  favorite: boolean
  archived: boolean
  hidden: boolean
  can_edit: boolean
  userVacancyBanHref: string
  quickResponseHref: string
  reactions: {
    items: {
      name: string
      title: string
      hasReacted: boolean
      count: number
      image: string
    }[]
    fallbackHref: string
  }
}

const VacancyJSONScheme: JTDSchemaType<IVacancyJSONScheme> = {
  properties: {
    id: {
      type: 'uint32',
    },
    href: {
      type: 'string',
    },
    title: {
      type: 'string',
    },
    isMarked: {
      type: 'boolean',
    },
    remoteWork: {
      type: 'boolean',
    },
    salaryQualification: {
      properties: {
        title: {
          type: 'string',
        },
        href: {
          type: 'string',
        },
      },
      nullable: true,
    },
    publishedDate: {
      properties: {
        date: {
          type: 'string',
        },
        title: {
          type: 'string',
        },
      },
    },
    company: {
      properties: {
        alias_name: {
          type: 'string',
        },
        href: {
          type: 'string',
        },
        title: {
          type: 'string',
        },
        accredited: {
          type: 'boolean',
        },
        logo: {
          properties: {
            src: {
              type: 'string',
            },
          },
        },
        rating: {
          properties: {
            title: {
              type: 'string',
            },
            value: {
              type: 'string',
            },
            href: {
              type: 'string',
            },
          },
          nullable: true,
        },
      },
    },
    employment: {
      enum: ['full_time', 'part_time'],
      nullable: true,
    },
    salary: {
      properties: {
        from: {
          type: 'uint32',
          nullable: true,
        },
        to: {
          type: 'uint32',
          nullable: true,
        },
        currency: {
          type: 'string',
        },
        formatted: {
          type: 'string',
        },
      },
    },
    divisions: {
      elements: {
        properties: {
          title: {
            type: 'string',
          },
          href: {
            type: 'string',
          },
        },
      },
    },
    skills: {
      elements: {
        properties: {
          title: {
            type: 'string',
          },
          href: {
            type: 'string',
          },
        },
      },
    },
    locations: {
      elements: {
        properties: {
          title: {
            type: 'string',
          },
          href: {
            type: 'string',
          },
        },
      },
      nullable: true,
    },
    favorite: {
      type: 'boolean',
    },
    archived: {
      type: 'boolean',
    },
    hidden: {
      type: 'boolean',
    },
    can_edit: {
      type: 'boolean',
    },
    userVacancyBanHref: {
      type: 'string',
    },
    quickResponseHref: {
      type: 'string',
    },
    reactions: {
      properties: {
        items: {
          elements: {
            properties: {
              name: {
                type: 'string',
              },
              title: {
                type: 'string',
              },
              hasReacted: {
                type: 'boolean',
              },
              count: {
                type: 'uint16',
              },
              image: {
                type: 'string',
              },
            },
          },
        },
        fallbackHref: {
          type: 'string',
        },
      },
    },
  },
  optionalProperties: {
    location: {
      nullable: true,
    },
    media: {
      nullable: true,
    },
  },
}

export default function vacancies(ajv: Ajv) {
  return {
    vacancy: ajv.compile(VacancyJSONScheme),
  }
}
