import Ajv from 'ajv'
import { JTDSchemaType } from 'ajv/dist/core'

interface IResumeJSONScheme {
  id: string
  title: string
  href: string
  conversationHref: string | null
  avatar: {
    src: string
    src2x: string
  }
  lastVisited: {
    title: string
    date: string
  }
  specialization: string
  qualification: {
    title: string
    value: number
  } | null
  salary: {
    title: string
    value: number
    currency: string
  } | null
  availability: {
    title: string
    value: string
  }
  location: {
    title: string
    name: string
    href: string
    value: number
  } | null
  remoteWork: boolean
  relocation: boolean
  skills: {
    title: string
    href: string
    value: number
  }[]
  age: {
    title: string
    value: number
  } | null
  experience: {
    title: string
    value: number
  }
  lastJob: {
    position: string
    company: {
      title: string
      accredited?: boolean
      href: string | null
    }
    duration: {
      title: string
      value: number
    }
  } | null
  education: {
    university: {
      title: string
      href: string
    }
    faculty: string
    duration: {
      title: string
      value: number
    }
  } | null
  additionalEducation: {
    title: string
    href?: string
  }[]
  communities: {
    title: string
    name: string
    icon: string
    href: string
  }[]
  coworkers: { some: string }[]
  specializations: {
    title: string
  }[]
  gender: number
  isExpert: boolean
  moreUniversityCount: number
  companiesCount: string
  companiesHistory: { some: string }[]
}

const ResumeJSONScheme: JTDSchemaType<IResumeJSONScheme> = {
  properties: {
    id: {
      type: 'string',
    },
    title: {
      type: 'string',
    },
    href: {
      type: 'string',
    },
    conversationHref: {
      type: 'string',
      nullable: true,
    },
    avatar: {
      properties: {
        src: {
          type: 'string',
        },
        src2x: {
          type: 'string',
        },
      },
    },
    lastVisited: {
      properties: {
        title: {
          type: 'string',
        },
        date: {
          type: 'string',
        },
      },
    },
    specialization: {
      type: 'string',
    },
    qualification: {
      properties: {
        title: {
          type: 'string',
        },
        value: {
          type: 'int16',
        },
      },
      nullable: true,
    },
    salary: {
      properties: {
        title: {
          type: 'string',
        },
        value: {
          type: 'uint32',
        },
        currency: {
          type: 'string',
        },
      },
      nullable: true,
    },
    availability: {
      properties: {
        title: {
          type: 'string',
        },
        value: {
          type: 'string',
        },
      },
    },
    location: {
      properties: {
        title: {
          type: 'string',
        },
        name: {
          type: 'string',
        },
        href: {
          type: 'string',
        },
        value: {
          type: 'int32',
        },
      },
      nullable: true,
    },
    remoteWork: {
      type: 'boolean',
    },
    relocation: {
      type: 'boolean',
    },
    skills: {
      elements: {
        properties: {
          title: {
            type: 'string',
          },
          href: {
            type: 'string',
          },
          value: {
            type: 'int32',
          },
        },
      },
    },
    age: {
      properties: {
        title: {
          type: 'string',
        },
        value: {
          type: 'uint8',
        },
      },
      nullable: true,
    },
    experience: {
      properties: {
        title: {
          type: 'string',
        },
        value: {
          type: 'uint16',
        },
      },
    },
    lastJob: {
      properties: {
        position: {
          type: 'string',
        },
        company: {
          properties: {
            title: {
              type: 'string',
            },
            href: {
              type: 'string',
              nullable: true,
            },
          },
          optionalProperties: {
            accredited: {
              type: 'boolean',
            },
          },
        },
        duration: {
          properties: {
            title: {
              type: 'string',
            },
            value: {
              type: 'uint16',
            },
          },
        },
      },
      nullable: true,
    },
    education: {
      properties: {
        university: {
          properties: {
            title: {
              type: 'string',
            },
            href: {
              type: 'string',
            },
          },
        },
        faculty: {
          type: 'string',
        },
        duration: {
          properties: {
            title: {
              type: 'string',
            },
            value: {
              type: 'uint16',
            },
          },
        },
      },
      nullable: true,
    },
    additionalEducation: {
      elements: {
        properties: {
          title: {
            type: 'string',
          },
        },
        optionalProperties: {
          href: {
            type: 'string',
          },
        },
      },
    },
    communities: {
      elements: {
        properties: {
          href: {
            type: 'string',
          },
          icon: {
            type: 'string',
          },
          name: {
            type: 'string',
          },
          title: {
            type: 'string',
          },
        },
      },
    },
    coworkers: {
      elements: {
        properties: {
          some: {
            type: 'string',
          },
        },
      },
    },
    specializations: {
      elements: {
        properties: {
          title: {
            type: 'string',
          },
        },
      },
    },
    gender: {
      type: 'int8',
    },
    isExpert: {
      type: 'boolean',
    },
    moreUniversityCount: {
      type: 'int32',
    },
    companiesCount: {
      type: 'string',
    },
    companiesHistory: {
      elements: {
        properties: {
          some: {
            type: 'string',
          },
        },
      },
    },
  },
}

export default function resumes(ajv: Ajv) {
  return {
    resume: ajv.compile(ResumeJSONScheme),
  }
}
