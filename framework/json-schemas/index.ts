import Ajv from 'ajv/dist/jtd'
import addFormats from 'ajv-formats'
import vacancies from './vacancies'
import resumes from './resumes'
import experts from './experts'
import habrResumes from './habrResumes'
import qaResumes from './qaResumes'

// Don't use default values explicitly in config object.
// It's singleton class instance for memory savings.
const ajv = new Ajv({
  strictTypes: true,
  strictTuples: true,
  strictRequired: true,
  allErrors: true,
  timestamp: 'string', // timestamp is string type
  parseDate: true, // date-time string is Date type
})

addFormats(ajv, {
  keywords: true,
  formats: ['date-time'],
})

export default {
  vacancies: vacancies(ajv),
  resumes: resumes(ajv),
  habrResumes: habrResumes(ajv),
  qaResumes: qaResumes(ajv),
  experts: experts(ajv),
}
