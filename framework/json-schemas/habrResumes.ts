import Ajv from 'ajv'
import { JTDSchemaType } from 'ajv/dist/core'

interface IHabrResumeJSONScheme {
  id: string
  title: string
  href: string
  careerHref: string | null
  avatar: {
    src: string
  }
  lastVisited: {
    title: string
    date: string
  }
  specialization: string | null
  location: {
    title: string
    name: string
    href: string
    value: number
  } | null
  companies:
    | {
        title: string
        href: string
      }[]
    | null
  habr: {
    user: {
      title: string
      href: string
    }
    activity: {
      title: string
      items: {
        title: string
        href: string
        highlighted: boolean
        rank?: {
          title: string
          level: string
        }
      }[]
    }
    subscriptions: {
      title: string
      items: {
        title: string
        href: string
        highlighted: boolean
      }[]
    }
  }
  privateInfo: string | null
  qa: {
    user: {
      title: string
      href: string
    }
    activity: {
      title: string
      items: {
        title: string
        href: string
        highlighted: boolean
      }[]
    } | null
  } | null
}

const HabrResumeJSONScheme: JTDSchemaType<IHabrResumeJSONScheme> = {
  properties: {
    avatar: {
      properties: {
        src: {
          type: 'string',
        },
      },
    },
    careerHref: {
      type: 'string',
      nullable: true,
    },
    companies: {
      elements: {
        properties: {
          href: {
            type: 'string',
          },
          title: {
            type: 'string',
          },
        },
      },
      nullable: true,
    },
    habr: {
      properties: {
        subscriptions: {
          properties: {
            items: {
              elements: {
                properties: {
                  highlighted: {
                    type: 'boolean',
                  },
                  href: {
                    type: 'string',
                  },
                  title: {
                    type: 'string',
                  },
                },
              },
            },
            title: {
              type: 'string',
            },
          },
        },
        user: {
          properties: {
            href: {
              type: 'string',
            },
            title: {
              type: 'string',
            },
          },
        },
        activity: {
          properties: {
            title: {
              type: 'string',
            },
            items: {
              elements: {
                properties: {
                  title: {
                    type: 'string',
                  },
                  href: {
                    type: 'string',
                  },
                  highlighted: {
                    type: 'boolean',
                  },
                },
                optionalProperties: {
                  rank: {
                    properties: {
                      title: {
                        type: 'string',
                      },
                      level: {
                        type: 'string',
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
    href: {
      type: 'string',
    },
    id: {
      type: 'string',
    },
    lastVisited: {
      properties: {
        date: {
          type: 'string',
        },
        title: {
          type: 'string',
        },
      },
    },
    location: {
      properties: {
        title: {
          type: 'string',
        },
        name: {
          type: 'string',
        },
        href: {
          type: 'string',
        },
        value: {
          type: 'uint32',
        },
      },
      nullable: true,
    },
    privateInfo: {
      nullable: true,
      type: 'string',
    },
    qa: {
      properties: {
        activity: {
          properties: {
            items: {
              elements: {
                properties: {
                  highlighted: {
                    type: 'boolean',
                  },
                  href: {
                    type: 'string',
                  },
                  title: {
                    type: 'string',
                  },
                },
              },
            },
            title: {
              type: 'string',
            },
          },
          nullable: true,
        },
        user: {
          properties: {
            href: {
              type: 'string',
            },
            title: {
              type: 'string',
            },
          },
        },
      },
      nullable: true,
    },
    specialization: {
      type: 'string',
      nullable: true,
    },
    title: {
      type: 'string',
    },
  },
}

export default function habrResumes(ajv: Ajv) {
  return {
    resume: ajv.compile(HabrResumeJSONScheme),
  }
}
