import { Locator } from '@playwright/test'
import VacancyPage from '../pages/Vacancy'

export const vacancyCardLocators = {
  card: '.vacancy-card__inner',
  titleLink: '.vacancy-card__info > .vacancy-card__title > .vacancy-card__title-link',
  basicSalary: '.basic-salary',
}

export default class VacancyCard {
  card: Locator

  constructor(locator: Locator) {
    this.card = locator
  }

  get titleLink(): Locator {
    return this.card.locator(vacancyCardLocators.titleLink)
  }
  get basicSalary() {
    return this.card.locator(vacancyCardLocators.basicSalary)
  }

  async openVacancyPage(): Promise<VacancyPage> {
    await this.titleLink.click()
    await this.card.page().waitForLoadState('domcontentloaded')
    return new VacancyPage(this.card.page())
  }
}
