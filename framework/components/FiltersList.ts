import { Locator, expect } from '@playwright/test'

export const filtersListLocators = {
  specsField: '.specs-picker__specs',
  showBasicSalaryCheckbox:
    '.content-section:nth-child(4) > .input-group > span.checkbox > .checkbox__wrapper',

  specsModal: '.overlay',
  specsModalFirstGroup: '.specs-selector__group-wrapper:nth-child(1)',
  specsModalFirstGroupItem: '.specs-selector__category:nth-child(1) > div > label',
  specsModalAcceptButton: '.button-group > button.button-comp--appearance-primary',
}

export default class FiltersList {
  list: Locator

  constructor(locator: Locator) {
    this.list = locator
  }

  get specsField() {
    return this.list.locator(filtersListLocators.specsField)
  }

  get showBasicSalaryCheckbox() {
    return this.list.locator(filtersListLocators.showBasicSalaryCheckbox)
  }

  get specsModal() {
    return this.list.locator(filtersListLocators.specsModal)
  }

  get specsModalFirstGroup() {
    return this.list.locator(filtersListLocators.specsModalFirstGroup)
  }

  get specsModalFirstGroupItem() {
    return this.list.locator(filtersListLocators.specsModalFirstGroupItem).first()
  }

  get specsModalAcceptButton() {
    return this.list.locator(filtersListLocators.specsModalAcceptButton)
  }

  async openSpecsModal() {
    await this.specsField.click()
  }

  async selectFirstSpecsFilter() {
    await this.openSpecsModal()
    await expect(this.specsField).toBeVisible()
    await this.specsModalFirstGroup.click()
    await expect(this.specsModalFirstGroupItem).toBeVisible()
    await this.specsModalFirstGroupItem.click()
    await this.specsModalAcceptButton.click()
    await expect(this.specsModal).toBeHidden()
  }

  async toggleShowBasicSalary() {
    await this.showBasicSalaryCheckbox.click()
  }
}
