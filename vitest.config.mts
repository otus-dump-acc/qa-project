// Comment these because it's better to do in npm script/ It's can be more convinient way for
// working qa and devs in one project.

import { defineConfig } from 'vitest/config'
import dotenv from 'dotenv'

dotenv.config()

export default defineConfig({
  test: {
    include: ['**/tests/api/*.{test,spec}.?(c|m)[jt]s?(x)'],
    testTimeout: 10000,
  },
})
